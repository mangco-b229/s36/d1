const express = require('express')
const mongoose = require('mongoose')
const taskRoute = require('./routes/taskRoute')

const app = express()
const port = 4001

mongoose.set('strictQuery', true)

app.use(express.json())
app.use(express.urlencoded({ extended: true }))


//MIDDLEWARES
app.use('/tasks', taskRoute)

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.n5pfk4s.mongodb.net/S36?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on("error", console.error.bind(console, "Connection Error"))

db.once("open", () => console.log("We're connected to the cloud database."));


app.listen(port, () => console.log(`Now listening to port ${port}`))