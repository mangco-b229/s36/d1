const Task = require('../models/Task')


// GET ALL TASKS
module.exports.getAllTasks = () => {
	return Task.find({}).then(results => {
		return results
	})
}

// CREATE A TASK
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return task;
		}
	})
}


// DELETE TASK
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}


// UPDATE TASK
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		}

		result.name = newContent.name

		return result.save().then((updatedTask, err) => {
			if(err) {
				console.log(err)
			} else {
				return updatedTask
			}
		})
	})
}


// GET TASK
module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return result
		}

	})
}

module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} 

		result.status = "complete"

		return result.save().then((updatedTask,err) => {
			if(err) {
				console.log(err)
				return false
			} else {
				return updatedTask
			}
		})
	})
}