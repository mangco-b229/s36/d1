const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskController')

//[SECTION] - Routes

router.get('/', (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

router.post('/', (req, res) => {
	taskController.createTask(req.body)
		.then(resultFromController => res.send(resultFromController))
})

router.delete('/:id', (req, res) => {
	taskController.deleteTask(req.params.id)
		.then(resultFromController => res.send(resultFromController))
})

router.put('/:id', (req, res) => {
	taskController.updateTask(req.params.id, req.body) 
		.then(resultFromController => res.send(resultFromController))	
})

router.get('/:id', (req, res) => {
	taskController.getTask(req.params.id) 
		.then(resultFromController => res.send(resultFromController))	
})

router.put('/:id/complete', (req, res) => {
	taskController.completeTask(req.params.id)
		.then(resultFromController => res.send(resultFromController))
})

module.exports = router